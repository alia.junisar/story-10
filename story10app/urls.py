from django.urls import path
from . import views

app_name = 'story10app'

urlpatterns = [
	path('', views.index, name='index'),
]