from django.shortcuts import render

# Create your views here.

def index(request):
	host = request.get_host()
	port = request.get_port()
	return render(request, 'index.html', {'host':host, 'port':port})