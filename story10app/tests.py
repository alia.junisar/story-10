from django.test import TestCase
from django.test import Client
from django.urls import reverse
from django.urls import resolve
from .views import index
from .apps import Story10AppConfig

class Story10TestCase(TestCase):

	def test_home_exists(self):
		response = self.client.get(reverse('story10app:index'))
		self.assertEqual(response.status_code, 200)